package com.example.spaceinvaders20;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.spaceinvaders20.fragments.HomeFragment;
import com.example.spaceinvaders20.utilities.FragmentHandler;
import com.example.spaceinvaders20.utilities.HomePagerAdapter;
import com.example.spaceinvaders20.utilities.NotificationService;

/**
 * MainActivity class.
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = "TAG_ACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(this.getSupportActionBar() != null) {
            this.getSupportActionBar().hide();
        } else {
            Log.e(TAG, "Support action bar equals to null");
        }
        FragmentHandler.replaceFragment(this, HomeFragment.newInstance(HomePagerAdapter.HOME_INDEX));
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.stopService(new Intent(this, NotificationService.class));
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.startService(new Intent(this, NotificationService.class));
    }
}
