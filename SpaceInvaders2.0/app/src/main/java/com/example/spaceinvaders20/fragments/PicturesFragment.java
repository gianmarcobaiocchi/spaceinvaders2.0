package com.example.spaceinvaders20.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.spaceinvaders20.R;
import com.example.spaceinvaders20.fragments.recyclerview.picture.PictureViewHolder;
import com.example.spaceinvaders20.utilities.FragmentHandler;
import com.example.spaceinvaders20.utilities.HomePagerAdapter;
import com.example.spaceinvaders20.fragments.recyclerview.picture.PictureAdapter;

/**
 * Profile picture picker fragment.
 */
public class PicturesFragment extends Fragment {

    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.image_pick_fragment, container, false);
        this.activity = this.getActivity();
        if(this.activity != null){
            //Setting RecyclerView
            RecyclerView recyclerView = view.findViewById(R.id.imageRecyclerView);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new GridLayoutManager(this.getContext(), 2));
            recyclerView.setAdapter(new PictureAdapter(this.activity, position -> {
                final String currentImage = "picture_" + (position + 1);
                SharedPreferences sharedPreferences = this.activity.getSharedPreferences(this.getString(R.string.PLAYER_PREFERENCES), Context.MODE_PRIVATE);
                String oldId = sharedPreferences.getString(this.getString(R.string.PLAYER_PICTURE_KEY), "");
                if(!oldId.isEmpty()){
                    int index = Integer.valueOf(oldId.replace("picture_", "")) - 1;
                    ((PictureViewHolder) recyclerView.getChildViewHolder(recyclerView.getChildAt(index))).setEnabled(true);
                }
                sharedPreferences.edit()
                                 .putString(this.getString(R.string.PLAYER_PICTURE_KEY), currentImage)
                                 .apply();
                ((PictureViewHolder) recyclerView.getChildViewHolder(recyclerView.getChildAt(position))).setEnabled(false);
            }));
            //Set back button behaviour
            OnBackPressedCallback callback = new OnBackPressedCallback(true) {
                @Override
                public void handleOnBackPressed() {
                    FragmentHandler.replaceFragment((FragmentActivity) activity, HomeFragment.newInstance(HomePagerAdapter.PROFILE_INDEX));
                }
            };
            this.requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
            view.findViewById(R.id.backButton).setOnClickListener(v -> FragmentHandler.replaceFragment((FragmentActivity) this.activity, HomeFragment.newInstance(HomePagerAdapter.PROFILE_INDEX)));
        }
        return view;
    }
}
