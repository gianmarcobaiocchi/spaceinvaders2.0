package com.example.spaceinvaders20.utilities.database;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.Future;

/**
 * Leaderboard Room repository.
 */
public class LeaderboardRepository {

    private LeaderboardDAO leaderboardDAO;
    private LiveData<List<RankingPosition>> positions;

    /***
     * Builder.
     * @param application : runtime application.
     */
    public LeaderboardRepository(Application application){
        this.leaderboardDAO = LeaderboardRoomDatabase.getInstance(application).leaderboardDAO();
        this.positions = leaderboardDAO.getLeaderboard();
    }

    /**
     * All leaderboard positions' getter.
     * @return all leaderboard positions.
     */
    public LiveData<List<RankingPosition>> getPositions(){
        return this.positions;
    }

    /**
     * Adds a new scorer in the leaderboard.
     * @param position : new scorer.
     */
    public void addRankingPosition(RankingPosition position){
        LeaderboardRoomDatabase.DATABASE_WRITE_EXECUTORS.execute(() -> leaderboardDAO.addRankingPosition(position));
    }

    /**
     * Top ten scorers' getter.
     * @return the top ten scorers.
     */
    public List<RankingPosition>  getTopTen(){
        Future<List<RankingPosition>> future;
        List<RankingPosition> rankingPositions = null;
        try{
            future = LeaderboardRoomDatabase.DATABASE_WRITE_EXECUTORS.submit(() -> leaderboardDAO.getTopTen());
            rankingPositions = future.get();
        }catch (Exception e ) {
            Log.e("LEADERBOARD_DB", e.toString());
        }
        return rankingPositions;
    }
}
