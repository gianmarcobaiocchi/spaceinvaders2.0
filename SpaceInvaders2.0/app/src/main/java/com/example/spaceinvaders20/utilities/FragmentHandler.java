package com.example.spaceinvaders20.utilities;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.spaceinvaders20.R;

/**
 * Fragment handler class.
 */
public class FragmentHandler {

    /**
     * Replaces in the activity the actual fragment with another.
     * @param activity : where to place the fragment.
     * @param fragment : the fragment to adds in the activity.
     */
    public static void replaceFragment(FragmentActivity activity, Fragment fragment){
        FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment, fragment.getClass().getName());
        fragmentTransaction.commit();
    }

}
