package com.example.spaceinvaders20.utilities;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.MediaStore;
import android.view.PixelCopy;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.example.spaceinvaders20.R;
import com.google.ar.sceneform.ArSceneView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * Screen shot handler class.
 */
public class ScreenshotHandler {

    private final Activity activity;
    private final ArSceneView view;

    /**
     * ScreenshotHandler's builder.
     * @param activity : activity father.
     * @param view : ArSceneView where to take the picture.
     */
    public ScreenshotHandler(final Activity activity, final ArSceneView view){
        this.activity = activity;
        this.view = view;
    }

    /**
     * Takes a photo from the ArScene and saves it in the gallery.
     */
    public void takePhoto() {
        if(ContextCompat.checkSelfPermission(this.activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            final String filename = this.generateFilename();
            final Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),
                    Bitmap.Config.ARGB_8888);
            final HandlerThread handlerThread = new HandlerThread("PixelCopier");
            handlerThread.start();
            PixelCopy.request(view, bitmap, (copyResult) -> {
                if (copyResult == PixelCopy.SUCCESS) {
                    try {
                        saveBitmapToDisk(bitmap, filename);
                    } catch (IOException e) {
                        this.activity.runOnUiThread(() -> Toast.makeText(this.activity,
                                e.toString(), Toast.LENGTH_LONG).show());
                        return;
                    }
                    this.activity.runOnUiThread(() -> Toast.makeText(this.activity,
                            this.activity.getString(R.string.photo_saved), Toast.LENGTH_LONG).show());
                } else {
                    this.activity.runOnUiThread(() -> Toast.makeText(this.activity,
                            this.activity.getString(R.string.failed_copy_pixels, copyResult), Toast.LENGTH_LONG).show());
                }
                handlerThread.quitSafely();
            }, new Handler(handlerThread.getLooper()));
        } else {
            this.activity.runOnUiThread(() -> Toast.makeText(this.activity,
                    this.activity.getString(R.string.no_storage_permission), Toast.LENGTH_LONG).show());
        }
    }

    private String generateFilename() {
        String date = new SimpleDateFormat("yyyyMMddHHmmss", java.util.Locale.getDefault()).format(new Date());
        return Environment.DIRECTORY_PICTURES + File.separator  + "SpaceInvaders2.0" + File.separator + date + "_screenshot.jpg";
    }

    private Uri saveBitmapToDisk(Bitmap bitmap, String filename) throws IOException {
        File out = new File(filename);
        Uri imageUri;
        OutputStream fos;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ContentResolver resolver = this.activity.getContentResolver();
            ContentValues contentValues = new ContentValues();
            contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, filename.substring(filename.lastIndexOf(File.separator) + 1));
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg");
            contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, out.getParent());
            imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
            fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));
        } else {
            String imagesDir = out.getParent();
            File image = new File(imagesDir, filename.substring(filename.lastIndexOf(File.separator) + 1));
            imageUri = Uri.fromFile(image);
            fos = new FileOutputStream(image);
        }
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        Objects.requireNonNull(fos).close();
        return imageUri;
    }
}
