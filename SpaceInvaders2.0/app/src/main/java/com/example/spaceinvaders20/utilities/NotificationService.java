package com.example.spaceinvaders20.utilities;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.example.spaceinvaders20.MainActivity;
import com.example.spaceinvaders20.R;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * NotificationService manages background notification,
 * so to notify the user that there's a city to save from aliens.
 */
public class NotificationService extends Service {

    private static final String CHANNEL_ID = "Ufos attack";
    private static final int CHANCE = 15;
    private static final int PERIOD = 1000;
    private Timer timer;
    private TimerTask timerTask;

    @Override
    public void onDestroy() {
        this.stopTimerTask();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        this.startTimer();
        return START_STICKY;
    }

    private void startTimer() {
        this.timer = new Timer();
        this.initializeTimerTask();
        this.timer.schedule(this.timerTask, PERIOD, PERIOD);
    }

    private void stopTimerTask() {
        if(this.timer != null){
            this.timer.cancel();
            this.timer = null;
        }
    }

    private void initializeTimerTask() {
        this.timerTask = new TimerTask() {
            @Override
            public void run() {
                //Location permission check
                if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    LocationManager locationManager = ((LocationManager) getSystemService(Context.LOCATION_SERVICE));
                    //Control Gps Provider enabled
                    if (locationManager != null && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        //Probability control
                        if (new Random().nextInt(100) < CHANCE) {
                            NotificationManager notificationManager = getSystemService(NotificationManager.class);
                            if (notificationManager != null) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                    NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_ID, NotificationManager.IMPORTANCE_DEFAULT);
                                    channel.setDescription("Notify the user when a city need him help");
                                    notificationManager.createNotificationChannel(channel);
                                }
                                //Start application Intent
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);
                                Notification notification = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                                        .setSmallIcon(R.drawable.ufo)
                                        .setContentTitle(getResources().getString(R.string.app_name))
                                        .setContentText("There's a city to save!!")
                                        .setStyle(new NotificationCompat.BigTextStyle().bigText("There's a city to save!!"))
                                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                        .setAutoCancel(true)
                                        .setContentIntent(pendingIntent)
                                        .build();
                                notificationManager.notify((int) System.currentTimeMillis(), notification);
                            }
                            stopTimerTask();
                        }
                    }
                }
            }
        };
    }
}
