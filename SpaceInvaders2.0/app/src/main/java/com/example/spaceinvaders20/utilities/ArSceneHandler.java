package com.example.spaceinvaders20.utilities;

import android.app.Activity;
import android.graphics.Point;
import android.view.View;
import android.widget.Button;

import com.example.spaceinvaders20.R;
import com.example.spaceinvaders20.fragments.WriteArFragment;
import com.example.spaceinvaders20.game.objects.BulletObject;
import com.example.spaceinvaders20.game.objects.VirtualObject;
import com.google.ar.core.Anchor;
import com.google.ar.core.Frame;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.core.Trackable;
import com.google.ar.core.TrackingState;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.ArSceneView;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.collision.Ray;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ModelRenderable;

import java.util.List;
import java.util.Optional;

/**
 * ArScene handler class.
 */
public class ArSceneHandler {

    private static final String TAG = "AR_SCENE_ERROR";
    private final Activity activity;
    private final WriteArFragment writeArFragment;
    private Optional<Anchor> anchor;
    private boolean isTracking;
    private boolean isHitting;
    private boolean isCityPlaced;

    /**
     * ArSceneHandler's builder.
     * @param activity : activity father.
     * @param writeArFragment : ArFragment to manage.
     */
    public ArSceneHandler(Activity activity, WriteArFragment writeArFragment){
        this.activity = activity;
        this.writeArFragment = writeArFragment;
        this.anchor = Optional.empty();
        this.isTracking = false;
        this.isHitting = false;
        this.isCityPlaced = false;
        if (this.writeArFragment != null) {
            this.writeArFragment.getArSceneView().getScene().addOnUpdateListener(frameTime -> {
                this.writeArFragment.onUpdate(frameTime);
                this.onUpdate();
            });
        }
    }

    /**
     * Adds an object in Ar scene.
     * @param object : the VirtualObject to add in the scene.
     * @return true if added, false instead.
     */
    public boolean addObjectInScene(VirtualObject object){
        if(this.isHitting && !this.anchor.isPresent()){
            List<HitResult> hits;
            if(this.writeArFragment.getArSceneView().getArFrame() != null) {
                hits = this.writeArFragment.getArSceneView().getArFrame()
                           .hitTest(this.getScreenCenter().x, this.getScreenCenter().y);
                for(HitResult hit : hits){
                    Trackable trackable = hit.getTrackable();
                    if(trackable instanceof Plane && ((Plane)trackable).isPoseInPolygon(hit.getHitPose())){
                        this.anchor = Optional.of(hit.createAnchor());
                        this.loadModel(this.anchor.get(), object);
                        break;
                    }
                }
            }
        } else if(this.anchor.isPresent()){
            this.loadModel(this.anchor.get(), object);
        }
        return this.isHitting || this.anchor.isPresent();
    }

    /**
     * Adds a bullet in the scene.
     * @param bullet : bullet to add in the scene.
     */
    public void addBulletInScene(BulletObject bullet){
        ModelRenderable.builder()
                       .setSource(this.activity, bullet.getUri())
                       .build()
                       .handle((renderable, throwable) -> {
                           if(throwable == null){
                               bullet.getNode().setRenderable(renderable);
                               this.writeArFragment.getArSceneView().getScene().addChild(bullet.getNode());
                           }
                           return null;
                       });
    }

    /**
     * ArSceneView getter.
     * @return the ArFragment's ArSceneView.
     */
    public ArSceneView getArSceneView(){
        return this.writeArFragment.getArSceneView();
    }

    /**
     * Builds a ray that goes through the center of the screen.
     * @return the built ray.
     */
    public Ray getActualRay(){
        return this.writeArFragment.getArSceneView().getScene().getCamera().screenPointToRay(this.getScreenCenter().x, this.getScreenCenter().y);
    }

    /**
     * Sets if the city is built or not.
     * @param isCityPlaced : boolean value.
     */
    public void placeCity(final boolean isCityPlaced){
        this.isCityPlaced = isCityPlaced;
    }

    private void onUpdate(){
        this.updateTracking();
        this.updateHitTest();
        if(this.isTracking){
            Button placeButton = this.activity.findViewById(R.id.placeButton);
            if(this.isHitting && !this.isCityPlaced) {
                placeButton.setVisibility(View.VISIBLE);
                placeButton.invalidate();
            } else {
                placeButton.setVisibility(View.GONE);
            }
        }
    }

    private void loadModel(Anchor anchor, VirtualObject object){
        ModelRenderable.builder()
                .setSource(this.activity, object.getUri())
                .build()
                .handle((renderable, throwable) -> {
                    if(throwable == null){
                        this.addNodeToScene(anchor, renderable, object.getPosition(), object.getNode());
                    }
                    return null;
                });
    }

    private boolean updateTracking(){
        Frame frame = this.writeArFragment.getArSceneView().getArFrame();
        boolean wasTracking = isTracking;
        isTracking = frame != null && frame.getCamera().getTrackingState() == TrackingState.TRACKING;
        return isTracking != wasTracking;
    }

    private boolean updateHitTest(){
        Frame frame = this.writeArFragment.getArSceneView().getArFrame();
        android.graphics.Point pt = getScreenCenter();
        List<HitResult> hits;
        boolean wasHitting = this.isHitting;
        this.isHitting = false;
        if(frame!=null){
            hits = frame.hitTest(pt.x, pt.y);
            for(HitResult hit : hits){
                Trackable trackable = hit.getTrackable();
                if(trackable instanceof Plane && ((Plane) trackable).isPoseInPolygon(hit.getHitPose())){
                    this.isHitting = true;
                    break;
                }
            }
        }
        return wasHitting != this.isHitting;
    }

    private void addNodeToScene(final Anchor anchor, final ModelRenderable renderable, Vector3 position, Node node){
        AnchorNode anchorNode = new AnchorNode(anchor);
        node.setRenderable(renderable);
        node.setLocalPosition(position);
        node.setParent(anchorNode);
        this.writeArFragment.getArSceneView().getScene().addChild(anchorNode);
    }

    private Point getScreenCenter(){
        return new Point(this.writeArFragment.getArSceneView().getWidth() / 2, this.writeArFragment.getArSceneView().getHeight() / 2);
    }
}
