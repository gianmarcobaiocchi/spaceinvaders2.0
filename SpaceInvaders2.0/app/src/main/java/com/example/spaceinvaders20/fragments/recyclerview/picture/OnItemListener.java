package com.example.spaceinvaders20.fragments.recyclerview.picture;

/**
 * OnClick listener behaviour interface.
 */
public interface OnItemListener {
    /**
     * Click behaviour.
     * @param position : card's position.
     */
    void onItemClick(int position);
}
