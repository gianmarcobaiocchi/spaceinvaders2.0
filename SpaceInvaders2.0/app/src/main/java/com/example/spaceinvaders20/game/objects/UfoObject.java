package com.example.spaceinvaders20.game.objects;

import com.example.spaceinvaders20.game.GameWorldThread;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;

import java.util.Random;

/**
 * Ufo's class.
 */
public class UfoObject extends SimpleVirtualObject {

    /**
     * Position limit, both positive and negative.
     */
    public static final float POSITION_LIMIT = 0.4f;
    /**
     * Ufo max score point.
     */
    public static final int MAX_SCORE = 10;
    /**
     * Ufo min score point.
     */
    public static final int MIN_SCORE = 1;
    /**
     * Ufo max speed.
     */
    public static final float MAX_SPEED = 0.0005f;
    /**
     * Ufo min speed.
     */
    public static final float MIN_SPEED = 0.00005f;
    /**
     * Ufo max angle degree.
     */
    public static final int MAX_ANGLE_DEGREES = 30;
    private float speed;
    private final GameWorldThread world;
    private final int score;
    private float signX;
    private float signY;

    /**
     * UfoObject's builder.
     * @param path : resource path.
     * @param speed : ufo's speed (meter/milliseconds)
     * @param rotation : ufo's rotation.
     * @param position : ufo's local position.
     * @param world : the GameWorldThread in which it is created.
     * @param score : the ufo's score value.
     */
    public UfoObject(final String path, final float speed, final Vector3 rotation, final Vector3 position, final GameWorldThread world, final int score){
        super(path, position);
        super.getNode().setLocalRotation(Quaternion.axisAngle(rotation, 1));
        this.speed = speed;
        this.world = world;
        this.score = score;
        this.signX = new Random().nextInt(3) - 1;
        this.signY = new Random().nextInt(3) - 1;
    }

    @Override
    public void tick(final long millis) {
        //Down movement
        Vector3 pos = super.getPosition();
        if (pos.y >= 0){
            pos.y = pos.y - (this.speed * millis);
            pos.x = pos.x + this.signX * this.speed * millis;
            if(pos.x > POSITION_LIMIT){
                pos.x = POSITION_LIMIT;
                this.signX = -1;
            } else if(pos.x < -POSITION_LIMIT){
                pos.x = -POSITION_LIMIT;
                this.signX = 1;
            }
            pos.z = pos.z + this.signY * this.speed * millis;
            if(pos.z > POSITION_LIMIT){
                pos.z = POSITION_LIMIT;
                this.signY = -1;
            } else if(pos.z < -POSITION_LIMIT){
                pos.z = -POSITION_LIMIT;
                this.signY = 1;
            }
            this.setPosition(pos);
        } else {
            this.world.removeObject(super.getNode());
        }
    }

    /**
     * Ufo's score value getter.
     * @return the ufo's score value.
     */
    public int getScore() {
        return this.score;
    }
}
