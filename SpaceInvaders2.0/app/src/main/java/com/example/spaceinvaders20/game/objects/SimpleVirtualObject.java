package com.example.spaceinvaders20.game.objects;

import android.net.Uri;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Vector3;

/**
 * Git game object.
 */
public class SimpleVirtualObject implements VirtualObject{

    public static final float MAX_HEIGHT = 2.0f;
    private final Uri uri;
    private Vector3 position;
    private final Node node;

    /**
     * SimpleVirtualObject's builder.
     * @param path : resource path.
     */
    public SimpleVirtualObject(final String path){
        this.uri = Uri.parse(path);
        this.position = new Vector3(0, 0, 0);
        this.node = new Node();
    }

    /**
     * SimpleVirtualObject's builder.
     * @param path : resource path.
     * @param position : object's local position.
     */
    public SimpleVirtualObject(final String path, final Vector3 position){
        this.uri = Uri.parse(path);
        this.position = position;
        this.node = new Node();
    }

    @Override
    public synchronized void setPosition(Vector3 point) {
        this.position = point;
        this.node.setLocalPosition(this.position);
    }

    @Override
    public synchronized Uri getUri() {
        return this.uri;
    }

    @Override
    public synchronized Vector3 getPosition() {
        return new Vector3(this.position);
    }

    @Override
    public synchronized Node getNode() {
        return this.node;
    }

    @Override
    public void tick(final long millis) { }

    @Override
    public synchronized void destroy() {
        if(this.node != null){
            Node parent = this.node.getParent();
            if (parent != null) {
                parent.removeChild(this.node);
            }
        }
    }
}
