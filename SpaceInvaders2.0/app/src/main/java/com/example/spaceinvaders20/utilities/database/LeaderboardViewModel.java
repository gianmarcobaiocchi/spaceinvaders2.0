package com.example.spaceinvaders20.utilities.database;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

/**
 * Leaderboard Room Database ViewModel.
 */
public class LeaderboardViewModel extends AndroidViewModel {

    private LeaderboardRepository repository;
    private LiveData<List<RankingPosition>> leaderboard;

    /**
     * Builder.
     * @param application : runtime application.
     */
    public LeaderboardViewModel(@NonNull Application application) {
        super(application);
        this.repository = new LeaderboardRepository(application);
        this.leaderboard = repository.getPositions();
    }

    /**
     * All leaderboard positions' getter.
     * @return
     */
    public LiveData<List<RankingPosition>> getLeaderboard(){
        return this.leaderboard;
    }

    /**
     * Adds a scorer in the leaderboard.
     * @param position : new scorer.
     */
    public void addRankingPosition(final RankingPosition position){
        this.repository.addRankingPosition(position);
    }

    /**
     * Top ten scorers' getter.
     * @return the top ten scorers.
     */
    public List<RankingPosition> getTopTen(){
        return this.repository.getTopTen();
    }
}
