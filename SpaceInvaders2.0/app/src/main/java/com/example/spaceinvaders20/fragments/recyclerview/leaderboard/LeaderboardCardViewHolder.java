package com.example.spaceinvaders20.fragments.recyclerview.leaderboard;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.spaceinvaders20.R;

/**
 * Leaderboard RecyclerView's LeaderboardCardViewHolder.
 */
public class LeaderboardCardViewHolder extends RecyclerView.ViewHolder {

    private TextView positionTextView;
    private ImageView iconImageView;
    private TextView nameTextView;
    private TextView scoreTextView;

    /**
     * Builder.
     * @param itemView : card's view.
     */
    public LeaderboardCardViewHolder(@NonNull View itemView) {
        super(itemView);
        this.positionTextView = itemView.findViewById(R.id.positionTextView);
        this.iconImageView = itemView.findViewById(R.id.iconImageView);
        this.nameTextView = itemView.findViewById(R.id.nameEditText);
        this.scoreTextView = itemView.findViewById(R.id.scoreTextView);
    }

    /**
     * Image's getter.
     * @return scorer image.
     */
    public ImageView getIconImageView() {
        return iconImageView;
    }

    /**
     * Position's getter.
     * @return scorer position.
     */
    public TextView getPositionTextView() {
        return positionTextView;
    }

    /**
     * Scorer name's getter.
     * @return scorer name.
     */
    public TextView getNameTextView() {
        return nameTextView;
    }

    /**
     * Score's getter.
     * @return scorer points.
     */
    public TextView getScoreTextView() {
        return scoreTextView;
    }
}
