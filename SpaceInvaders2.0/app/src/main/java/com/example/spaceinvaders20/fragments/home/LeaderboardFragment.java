package com.example.spaceinvaders20.fragments.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.spaceinvaders20.R;
import com.example.spaceinvaders20.fragments.recyclerview.leaderboard.LeaderboardCardAdapter;
import com.example.spaceinvaders20.utilities.database.LeaderboardViewModel;

/**
 * Leaderboard's fragment
 */
public class LeaderboardFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.leaderboard_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(this.getActivity() != null) {
            RecyclerView recyclerView = this.getActivity().findViewById(R.id.leaderboardRecyclerView);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(
                    new LeaderboardCardAdapter(
                            this.getActivity(),
                            new ViewModelProvider(this).get(LeaderboardViewModel.class)));
        }
    }
}
