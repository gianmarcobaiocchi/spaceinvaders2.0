package com.example.spaceinvaders20.utilities;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.spaceinvaders20.fragments.home.ProfileFragment;
import com.example.spaceinvaders20.fragments.home.StartFragment;
import com.example.spaceinvaders20.fragments.home.LeaderboardFragment;

/**
 * HomePager tabs adapter class.
 */
public class HomePagerAdapter extends FragmentStateAdapter {

    public static final int PROFILE_INDEX = 0;
    public static final int HOME_INDEX = 1;
    public static final int LEADERBOARD_INDEX = 2;
    public static final int N_TABS = 3;

    /**
     * HomePagerAdapter's builder.
     * @param fragmentActivity : the activity.
     */
    public HomePagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0:
                return new ProfileFragment();
            case 1:
                return new StartFragment();
            default:
                return new LeaderboardFragment();
        }
    }

    @Override
    public int getItemCount() {
        return N_TABS;
    }
}
