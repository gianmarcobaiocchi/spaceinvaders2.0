package com.example.spaceinvaders20.game;

import android.app.Activity;
import android.util.Log;

import com.example.spaceinvaders20.game.objects.BulletObject;
import com.example.spaceinvaders20.game.objects.UfoObject;
import com.example.spaceinvaders20.game.objects.VirtualObject;
import com.google.ar.sceneform.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Game world thread.
 */
public class GameWorldThread extends Thread {

    private static final String TAG = "GAME_WORLD";
    private static final int FRAME_RATE = 30;
    private static final long FRAME_PERIOD = (1000 / FRAME_RATE);
    private final Activity activity;
    private final PlayerGame playerGame;
    private List<VirtualObject> ufosList;
    private List<VirtualObject> bulletsList;
    private boolean isInPause;

    /**
     * GameWorldThread's builder.
     * @param activity : the activity father.
     * @param playerGame : the player game handler.
     */
    public GameWorldThread(final Activity activity, final PlayerGame playerGame){
        this.activity = activity;
        this.playerGame = playerGame;
        this.ufosList = new ArrayList<>();
        this.bulletsList = new ArrayList<>();
        this.isInPause = true;
    }

    /**
     * Adds an Ufo in the world.
     * @param ufoObject : the ufo to add.
     */
    public void addUfo(final UfoObject ufoObject){
        this.ufosList.add(ufoObject);
    }

    /**
     * Adds a bullet in the world.
     * @param bulletObject : the bullet to add.
     */
    public void addBullet(final BulletObject bulletObject){
        this.bulletsList.add(bulletObject);
    }

    /**
     * Pause setter.
     * @param isInPause : true if in pause, false instead.
     */
    public void setInPause(final boolean isInPause){
        this.isInPause = isInPause;
    }

    /**
     * In pause getter.
     * @return if the world is in pause or not.
     */
    public boolean isInPause(){
        return this.isInPause;
    }

    /**
     * Adds points to the player score.
     * @param points : the points to add.
     */
    public void addPoints(final int points){
        this.playerGame.addPoints(points);
    }

    /**
     * Removes an object from the world.
     * @param node : object's node.
     * @return the object's score value.
     */
    public int removeObject(final Node node){
        int score = 0;
        Optional<VirtualObject> objectToRemove = Optional.empty();
        List <VirtualObject> temp = new ArrayList<>(this.ufosList);
        temp.addAll(this.bulletsList);
        for (VirtualObject object : temp) {
            if(object.getNode().equals(node)){
                object.destroy();
                objectToRemove = Optional.of(object);
                if(object.getClass().equals(UfoObject.class)){
                    score = ((UfoObject) object).getScore();
                }
            }
        }
        objectToRemove.ifPresent(virtualObject -> {
            this.ufosList.remove(virtualObject);
            this.bulletsList.remove(virtualObject);
        });
        return score;
    }

    @Override
    public void start() {
        super.start();
        this.isInPause = false;
    }

    @Override
    public void run() {
        long tValue = System.currentTimeMillis();
        while (!Thread.currentThread().isInterrupted()) {
            if(!this.isInPause){
                if(this.ufosList.isEmpty()){
                    Thread.currentThread().interrupt();
                    this.playerGame.gameOver();
                }
                // Temporary list creation
                List<VirtualObject> tempList = new ArrayList<>(this.ufosList);
                tempList.addAll(this.bulletsList);
                this.activity.runOnUiThread(() -> tempList.forEach(o -> o.tick(FRAME_PERIOD)));
                this.activity.runOnUiThread(() -> this.playerGame.tick(FRAME_PERIOD));
                try {
                    long tCurrent = System.currentTimeMillis();
                    long tElapsed = tCurrent - tValue;
                    if(FRAME_PERIOD - tElapsed > 0){
                        Thread.sleep(FRAME_PERIOD - tElapsed);
                    }
                    tValue = System.currentTimeMillis();
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            } else {
                tValue = System.currentTimeMillis();
            }
        }
    }
}
