package com.example.spaceinvaders20.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.spaceinvaders20.utilities.ArSceneHandler;
import com.example.spaceinvaders20.R;
import com.example.spaceinvaders20.game.PlayerGame;
import com.example.spaceinvaders20.utilities.FragmentHandler;
import com.example.spaceinvaders20.utilities.HomePagerAdapter;
import com.example.spaceinvaders20.utilities.ScreenshotHandler;
import com.example.spaceinvaders20.utilities.database.LeaderboardViewModel;

/**
 * Main game fragment.
 */
public class ArGameFragment extends Fragment {

    private ScreenshotHandler screenshotHandler;
    private PlayerGame playerGame;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.ar_game_fragment, container, false);
        final FragmentActivity activity = this.getActivity();
        if(activity != null){
            ArSceneHandler arSceneHandler = new ArSceneHandler(activity,
                                                    (WriteArFragment) this.getChildFragmentManager()
                                                                          .findFragmentById(R.id.writeArFragment));
            this.screenshotHandler = new ScreenshotHandler(activity, arSceneHandler.getArSceneView());
            this.playerGame = new PlayerGame(activity,
                    activity.getSharedPreferences(this.getString(R.string.PLAYER_PREFERENCES), Context.MODE_PRIVATE)
                            .getString(this.getString(R.string.PLAYER_PICTURE_KEY), ""),
                    activity.getSharedPreferences(this.getString(R.string.PLAYER_PREFERENCES), Context.MODE_PRIVATE)
                            .getString(this.getString(R.string.PLAYER_NAME_KEY), ""),
                    arSceneHandler,
                    new ViewModelProvider(this).get(LeaderboardViewModel.class));
            //Place city button action listener
            view.findViewById(R.id.placeButton).setOnClickListener(v -> {
                this.playerGame.init();
                view.findViewById(R.id.placeButton).setVisibility(View.GONE);
                view.findViewById(R.id.startButton).setVisibility(View.VISIBLE);
            });
            //Start button action listener
            view.findViewById(R.id.startButton).setOnClickListener(v -> {
                this.playerGame.start();
                view.findViewById(R.id.startButton).setVisibility(View.GONE);
                view.findViewById(R.id.shootButton).setVisibility(View.VISIBLE);
                view.findViewById(R.id.gameToolbar).setVisibility(View.VISIBLE);
            });
            //Shoot button action listener
            this.playerGame.addShootButton(view.findViewById(R.id.shootButton));
            //Score textView
            this.playerGame.addTextViewScore(view.findViewById(R.id.scoreTextView));
            //Pause button action listener
            view.findViewById(R.id.pauseButton).setOnClickListener(v -> {
                if(this.playerGame.isInPause()){
                    this.playerGame.resume();
                    view.findViewById(R.id.pauseLayout).setVisibility(View.GONE);
                    ((ImageButton) view.findViewById(R.id.pauseButton)).setImageResource(android.R.drawable.ic_media_pause);
                } else {
                    this.playerGame.pause();
                    view.findViewById(R.id.pauseLayout).setVisibility(View.VISIBLE);
                    ((ImageButton) view.findViewById(R.id.pauseButton)).setImageResource(android.R.drawable.ic_media_play);
                }
            });
            //Home button listener
            view.findViewById(R.id.homeButton).setOnClickListener(v -> FragmentHandler.replaceFragment(activity, HomeFragment.newInstance(HomePagerAdapter.HOME_INDEX)));
            //Screen shot action listener
            view.findViewById(R.id.screenshotButton).setOnClickListener(v -> this.screenshotHandler.takePhoto());
            //Set back button behaviour
            OnBackPressedCallback callback = new OnBackPressedCallback(true) {
                @Override
                public void handleOnBackPressed() {
                    FragmentHandler.replaceFragment(activity, HomeFragment.newInstance(HomePagerAdapter.HOME_INDEX));
                }
            };
            this.requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
            //Move slowly advice
            Toast.makeText(this.getContext(), this.getString(R.string.move), Toast.LENGTH_LONG).show();
        }
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        if(this.getView() != null && !this.playerGame.isInPause()){
            this.playerGame.pause();
            this.getView().findViewById(R.id.pauseLayout).setVisibility(View.VISIBLE);
            ((ImageButton) this.getView().findViewById(R.id.pauseButton)).setImageResource(android.R.drawable.ic_media_play);
        }
    }
}
