package com.example.spaceinvaders20.fragments.recyclerview.leaderboard;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.spaceinvaders20.R;
import com.example.spaceinvaders20.utilities.database.LeaderboardViewModel;
import com.example.spaceinvaders20.utilities.database.RankingPosition;

import java.util.List;

/**
 * LeaderboardCardAdapter of leaderboard's RecyclerView.
 */
public class LeaderboardCardAdapter extends RecyclerView.Adapter<LeaderboardCardViewHolder> {

    //Top ten scorers
    private List<RankingPosition> positions;
    private Activity activity;

    public LeaderboardCardAdapter(final Activity activity, LeaderboardViewModel viewModel){
        this.activity = activity;
        this.positions = viewModel.getTopTen();
    }

    @NonNull
    @Override
    public LeaderboardCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.leaderboard_card,
                parent, false);
        return new LeaderboardCardViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull LeaderboardCardViewHolder holder, int position) {
        final RankingPosition currentPosition = this.positions.get(position);
        //Profile image
        int pictureId = this.activity.getResources().getIdentifier(currentPosition.getPictureId(), "drawable", this.activity.getPackageName());
        if(pictureId == 0) {
            pictureId = R.drawable.picture_6;
        }
        Drawable drawable = activity.getDrawable(pictureId);
        holder.getIconImageView().setImageDrawable(drawable);
        //Name
        String name = currentPosition.getPlayerName();
        name = name.length() > 14 ? name.substring(0, 12).concat("..") : name;
        holder.getNameTextView().setText(name);
        //Position
        holder.getPositionTextView().setText(Integer.toString(position + 1));
        //Score
        holder.getScoreTextView().setText(this.activity.getString(R.string.score, currentPosition.getScore()));
    }

    @Override
    public int getItemCount() {
        return this.positions.size();
    }
}
