package com.example.spaceinvaders20.utilities.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface LeaderboardDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addRankingPosition(RankingPosition position);

    @Query("SELECT * FROM leaderboard")
    LiveData<List<RankingPosition>> getLeaderboard();

    @Query("SELECT * FROM leaderboard ORDER BY score DESC LIMIT 10")
    List<RankingPosition> getTopTen();
}
