package com.example.spaceinvaders20.fragments.recyclerview.picture;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.spaceinvaders20.R;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Image picker RecyclerView's LeaderboardCardAdapter.
 */
public class PictureAdapter extends RecyclerView.Adapter<PictureViewHolder> {

    private List<Integer> images;
    private Activity activity;
    private OnItemListener itemListener;

    /**
     * Builder.
     * @param activity : parent activity.
     * @param itemListener : onClick listener.
     */
    public PictureAdapter(final Activity activity, final OnItemListener itemListener){
        this.activity = activity;
        this.images = IntStream.range(1, 9)
                               .boxed()
                               .collect(Collectors.toList());
        this.itemListener = itemListener;
    }

    @NonNull
    @Override
    public PictureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.picture_card,
                parent, false);
        return new PictureViewHolder(layoutView, this.itemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull PictureViewHolder holder, int position) {
        final String currentImage = "picture_" + (position + 1);
        int id = this.activity.getResources().getIdentifier(currentImage, "drawable", this.activity.getPackageName());
        holder.getImage().setImageDrawable(this.activity.getDrawable(id));
        String pictureDisabled = this.activity.getSharedPreferences(this.activity.getString(R.string.PLAYER_PREFERENCES), Context.MODE_PRIVATE)
                .getString(this.activity.getString(R.string.PLAYER_PICTURE_KEY), "");
        if(currentImage.equals(pictureDisabled)){
            holder.setEnabled(false);
        }
    }

    @Override
    public int getItemCount() {
        return this.images.size();
    }
}
