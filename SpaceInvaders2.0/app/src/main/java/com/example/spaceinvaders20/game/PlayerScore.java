package com.example.spaceinvaders20.game;

import java.io.Serializable;

/**
 * Player score handler class.
 */
public class PlayerScore implements Serializable {

    private String pictureId;
    private int score;
    private final String playerName;

    /**
     * PlayerScore's builder.
     * @param playerName : player name.
     */
    public PlayerScore(final String pictureId, final String playerName){
        this.pictureId = pictureId;
        this.score = 0;
        this.playerName = playerName;
    }

    /**
     * Adds point to the player score.
     * @param score : the points to add.
     */
    public void addScore(final int score){
        this.score += score;
    }

    /**
     * Player score getter.
     * @return the player score.
     */
    public int getScore(){
        return this.score;
    }

    /**
     * Player name getter.
     * @return the player name.
     */
    public String getPlayerName(){
        return this.playerName;
    }

    /**
     * Player id picture getter.
     * @return the player picture's id.
     */
    public String getPictureId(){
        return this.pictureId;
    }
}
