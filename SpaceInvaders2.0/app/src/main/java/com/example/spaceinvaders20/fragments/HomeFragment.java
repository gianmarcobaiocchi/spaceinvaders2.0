package com.example.spaceinvaders20.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.example.spaceinvaders20.R;
import com.example.spaceinvaders20.utilities.HomePagerAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

/**
 * Home page fragment.
 */
public class HomeFragment extends Fragment {

    private static final String SELECTED_TAB_KEY = "SELECTED_TAB";
    private int tabSelected;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.tabSelected = this.getArguments() != null ? this.getArguments().getInt(SELECTED_TAB_KEY, HomePagerAdapter.HOME_INDEX) : HomePagerAdapter.HOME_INDEX;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        ViewPager2 viewPager = view.findViewById(R.id.homeViewPager);
        if(this.getActivity() != null){
            viewPager.setAdapter(new HomePagerAdapter(this.getActivity()));
            viewPager.setUserInputEnabled(false); //Swiping disabled
            TabLayout tabLayout = view.findViewById(R.id.homeTabs);
            TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(
                    tabLayout, viewPager, (tab, position) -> {
                        switch (position){
                            case HomePagerAdapter.PROFILE_INDEX:
                                tab.setIcon(R.drawable.account_circle);
                                tab.setText(R.string.profile);
                                break;
                            case HomePagerAdapter.HOME_INDEX:
                                tab.setIcon(R.drawable.home);
                                tab.setText(R.string.home);
                                break;
                            default:
                                tab.setIcon(R.drawable.leaderboard);
                                tab.setText(R.string.leaderboard);
                                break;
                        }
                    }
            );
            tabLayoutMediator.attach();
            tabLayout.selectTab(tabLayout.getTabAt(this.tabSelected));
        }
        return view;
    }

    /**
     * Builder.
     * @param tabSelected : set this tab at the beginning.
     * @return : new HomeFragment instance.
     */
    public static HomeFragment newInstance(int tabSelected){
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putInt(SELECTED_TAB_KEY, tabSelected);
        fragment.setArguments(args);
        return fragment;
    }
}
