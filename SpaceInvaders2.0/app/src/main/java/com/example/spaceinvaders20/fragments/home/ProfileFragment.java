package com.example.spaceinvaders20.fragments.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.spaceinvaders20.R;
import com.example.spaceinvaders20.fragments.PicturesFragment;
import com.example.spaceinvaders20.utilities.FragmentHandler;

/**
 * Profile's fragment, where the player chooses his name and picture.
 */
public class ProfileFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.profile_fragment, container, false);
        if(this.getActivity() != null){
            //Player name
            final SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(this.getString(R.string.PLAYER_PREFERENCES), Context.MODE_PRIVATE);
            String playerName = sharedPreferences.getString(this.getString(R.string.PLAYER_NAME_KEY), "");
            ((EditText) view.findViewById(R.id.nameEditText)).setText(playerName);
            if(playerName.equals("")){
                ((Button) view.findViewById(R.id.editButton)).setText(R.string.register);
                view.findViewById(R.id.nameEditText).setEnabled(true);
            } else {
                ((Button) view.findViewById(R.id.editButton)).setText(R.string.edit);
                view.findViewById(R.id.nameEditText).setEnabled(false);
            }
            //Player picture
            String playerPicture = sharedPreferences.getString(this.getString(R.string.PLAYER_PICTURE_KEY), "");
            int idPicture = R.drawable.picture_6;
            if(!playerPicture.isEmpty()){
                idPicture = this.getActivity().getResources().getIdentifier(playerPicture, "drawable", this.getActivity().getPackageName());
            }
            ((ImageView) view.findViewById(R.id.profileImageView)).setImageDrawable(this.getActivity().getDrawable(idPicture));
            view.findViewById(R.id.profileImageView).setOnClickListener(v -> FragmentHandler.replaceFragment(this.getActivity(), new PicturesFragment()));
            view.findViewById(R.id.editButton).setOnClickListener(v -> {
                if(((Button) view.findViewById(R.id.editButton)).getText().equals(getString(R.string.edit))){
                    view.findViewById(R.id.nameEditText).setEnabled(true);
                    ((Button) view.findViewById(R.id.editButton)).setText(R.string.register);
                } else {
                    sharedPreferences.edit()
                               .putString(this.getString(R.string.PLAYER_NAME_KEY), ((EditText) view.findViewById(R.id.nameEditText)).getText().toString())
                               .apply();
                    view.findViewById(R.id.nameEditText).setEnabled(false);
                    ((Button) view.findViewById(R.id.editButton)).setText(R.string.edit);
                }
            });
        }
        return view;
    }
}
