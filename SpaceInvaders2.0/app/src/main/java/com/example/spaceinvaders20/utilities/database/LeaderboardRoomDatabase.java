package com.example.spaceinvaders20.utilities.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Leaderboard Database.
 */
@Database(entities = {RankingPosition.class}, version = 1, exportSchema = false)
abstract class LeaderboardRoomDatabase extends RoomDatabase {

    abstract LeaderboardDAO leaderboardDAO();

    private static LeaderboardRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREAD = 4;
    /**
     * Database thread pool.
     */
    public static final ExecutorService DATABASE_WRITE_EXECUTORS = Executors.newFixedThreadPool(NUMBER_OF_THREAD);

    /**
     * LeaderboardRoomDatabase instance getter.
     * @param context : runtime context.
     * @return LeaderboardRoomDatabase singleton.
     */
    public static LeaderboardRoomDatabase getInstance(Context context){
        if(INSTANCE == null){
            synchronized (LeaderboardRoomDatabase.class){
                if(INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), LeaderboardRoomDatabase.class, "LeaderboardDatabase").build();
                }
            }
        }
        return INSTANCE;
    }
}
