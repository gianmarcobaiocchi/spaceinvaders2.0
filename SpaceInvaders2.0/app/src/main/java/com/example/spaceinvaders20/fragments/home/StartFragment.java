package com.example.spaceinvaders20.fragments.home;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ScaleDrawable;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.example.spaceinvaders20.R;
import com.example.spaceinvaders20.fragments.ArGameFragment;
import com.example.spaceinvaders20.utilities.FragmentHandler;
import com.example.spaceinvaders20.utilities.HomePagerAdapter;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Home page's fragment for start to play to a new game.
 */
public class StartFragment extends Fragment {

    private static final double MAX_DISTANCE = 0.0001;
    private static final double MIN_DISTANCE = 0.00001;
    private static final int MAX_UFOS = 5;
    private static final String MAP_KEY = "OSM_MAP";
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;
    private View autorenewButton;
    private View myLocationButton;
    private MapView mapView;
    private MyLocationNewOverlay myLocation;
    private boolean isNetworkConnected;
    private boolean iconsAdded = false;
    private ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback(){
        @Override
        public void onAvailable(@NonNull Network network) {
            super.onAvailable(network);
            isNetworkConnected = true;
            Activity activity = getActivity();
            if(activity != null) {
                activity.runOnUiThread(() -> initMap());
            }
        }

        @Override
        public void onLost(@NonNull Network network) {
            super.onLost(network);
            isNetworkConnected = false;
            Activity activity = getActivity();
            if (activity != null) {
                activity.runOnUiThread(() -> resetMap());
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = null;
        Activity activity = this.getActivity();
        requestPermissionIfNecessary(new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_NETWORK_STATE});
        if(activity != null) {
            Context ctx = activity.getApplicationContext();
            Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
            view = inflater.inflate(R.layout.start_fragment, container, false);
            //MapView initialization
            this.mapView = view.findViewById(R.id.mapView);
            this.mapView.setDestroyMode(false);
            this.mapView.setMultiTouchControls(true);
            this.mapView.getController().setZoom(19.0);
            this.autorenewButton = view.findViewById(R.id.autorenewImageView);
            this.myLocationButton = view.findViewById(R.id.setPositionFloatingButton);
            this.autorenewButton.setOnClickListener(v -> this.initMap());
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        this.registerNetworkCallBack();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(this.getActivity() != null){
            Context ctx = this.getActivity().getApplicationContext();
            Configuration.getInstance().save(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
            if(this.mapView != null){
                this.mapView.onPause();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(this.getActivity() != null){
            Context ctx = this.getActivity().getApplicationContext();
            Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
            //Set start location
            if(this.mapView != null){
                IMapController mapController = this.mapView.getController();
                mapController.setZoom(19.0);
                if(this.myLocation != null) {
                    mapController.setCenter(this.myLocation.getMyLocation());
                }
                this.mapView.onResume();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Activity activity = this.getActivity();
        if(activity != null){
            ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            if(connectivityManager != null){
                connectivityManager.unregisterNetworkCallback(this.networkCallback);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        ArrayList<String> permissionsToRequest = new ArrayList<>(Arrays.asList(permissions).subList(0, grantResults.length));
        if(permissionsToRequest.size() > 0 && this.getActivity() != null){
            ActivityCompat.requestPermissions(this.getActivity(),
                    permissionsToRequest.toArray(new String[0]),
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void initMap(){
        Activity activity = this.getActivity();
        if(activity != null && !this.iconsAdded){
            if(this.isNetworkConnected){
                this.mapView.getOverlays().clear(); //Clear old overlays
                //Added location icon
                this.myLocation = new MyLocationNewOverlay(new GpsMyLocationProvider(activity.getApplicationContext())
                                                            , this.mapView);
                this.myLocation.enableMyLocation();
                this.mapView.getOverlays().add(this.myLocation);
                this.mapView.invalidate();
                this.myLocationButton.setOnClickListener(v -> {
                    if(this.isNetworkConnected){
                        this.myLocation.enableFollowLocation();
                    }
                });
                if(!this.myLocation.isMyLocationEnabled()){
                    Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content),
                            "Location is not available", Snackbar.LENGTH_LONG);
                    snackbar.setAction("Settings", v -> this.startIntent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)).show();
                } else {
                    //Added double tap rotation
                    RotationGestureOverlay rotationGestureOverlay = new RotationGestureOverlay(this.mapView);
                    rotationGestureOverlay.setEnabled(true);
                    this.mapView.getOverlays().add(rotationGestureOverlay);
                    this.myLocation.runOnFirstFix(this::generateUfoIcons);
                    this.myLocation.enableFollowLocation();
                    //Added start point
                    this.mapView.getController().setCenter(this.myLocation.getMyLocation());
                    //Start button action listener
                    this.autorenewButton.setVisibility(View.GONE);
                    this.iconsAdded = true;
                }
            } else {
                Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content),
                        "Network not available", Snackbar.LENGTH_LONG);
                snackbar.setAction("Settings", v -> this.startIntent(Settings.ACTION_WIRELESS_SETTINGS)).show();
            }
        }
    }

    private void resetMap(){
        this.mapView.getOverlays().clear();
        this.autorenewButton.setVisibility(View.VISIBLE);
        this.myLocation.disableFollowLocation();
        this.iconsAdded = false;
    }

    private void registerNetworkCallBack(){
        Activity activity = this.getActivity();
        if(activity != null){
            ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            if(connectivityManager != null){
                connectivityManager.registerDefaultNetworkCallback(this.networkCallback);
            }
        }
        this.isNetworkConnected = false;
    }

    private void startIntent(String action){
        Activity activity = this.getActivity();
        if(activity != null){
            Intent intent = new Intent();
            intent.setAction(action);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if(intent.resolveActivity(activity.getPackageManager()) != null){
                startActivity(intent);
            }
        }
    }

    private void requestPermissionIfNecessary(final String[] permissions){
        ArrayList<String> permissionsToRequest = new ArrayList<>();
        for (String permission : permissions) {
            if (this.getContext() != null
                    && ContextCompat.checkSelfPermission(this.getContext(), permission) != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                permissionsToRequest.add(permission);
            }
        }
        if (permissionsToRequest.size() > 0 && this.getActivity() != null) {
            ActivityCompat.requestPermissions(
                    this.getActivity(),
                    permissionsToRequest.toArray(new String[0]),
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void generateUfoIcons(){
        final int nUfos = new Random().nextInt(MAX_UFOS) + 1;
        final FragmentActivity activity = this.getActivity();
        if(activity != null && this.myLocation != null && this.myLocation.getMyLocation() != null) {
            for(int i = 0; i < nUfos; i++){
                Marker marker = new Marker(this.mapView);
                GeoPoint point = this.myLocation.getMyLocation();
                double latitudeOffset = new Random().nextDouble() % MAX_DISTANCE - MIN_DISTANCE;
                double longitudeOffset = new Random().nextDouble() % MAX_DISTANCE - MIN_DISTANCE;
                point.setCoords(point.getLatitude() - latitudeOffset, point.getLongitude() - longitudeOffset);
                Log.d(MAP_KEY, "Distance: " + point.distanceToAsDouble(this.myLocation.getMyLocation()));
                marker.setPosition(point);
                ScaleDrawable drawable = new ScaleDrawable(activity.getDrawable(R.drawable.ufo),
                        0, -1, -1);
                drawable.setLevel(1);
                marker.setIcon(drawable);
                marker.setOnMarkerClickListener((marker1, mapView1) -> {
                    if (marker1.getPosition().distanceToAsDouble(this.myLocation.getMyLocation()) <= 50) {
                        if (!this.getActivity().getSharedPreferences(this.getString(R.string.PLAYER_PREFERENCES), Context.MODE_PRIVATE)
                                .getString(this.getString(R.string.PLAYER_NAME_KEY), "").equals("")) {
                            FragmentHandler.replaceFragment(activity, new ArGameFragment());
                        } else {
                            Toast.makeText(activity, "Register name", Toast.LENGTH_LONG).show();
                            TabLayout tabLayout = activity.findViewById(R.id.homeTabs);
                            tabLayout.selectTab(tabLayout.getTabAt(HomePagerAdapter.PROFILE_INDEX));
                        }
                    } else {
                        Toast.makeText(activity, "City too far", Toast.LENGTH_LONG).show();
                    }
                    return true;
                });
                activity.runOnUiThread(() -> {
                    this.mapView.getOverlays().add(marker);
                    this.mapView.invalidate();
                });
            }
        }
    }
}
