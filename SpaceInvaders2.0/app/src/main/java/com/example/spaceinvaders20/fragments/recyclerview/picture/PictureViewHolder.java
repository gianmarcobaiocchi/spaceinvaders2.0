package com.example.spaceinvaders20.fragments.recyclerview.picture;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.spaceinvaders20.R;

/**
 * Image picker RecyclerView's ViewHolder.
 */
public class PictureViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private View itemView;
    private ImageView image;
    private ImageView checkImage;
    private OnItemListener itemListener;

    /**
     * Builder.
     * @param itemView : card's view.
     * @param itemListener : onClick listener.
     */
    public PictureViewHolder(@NonNull View itemView, final OnItemListener itemListener) {
        super(itemView);
        this.itemView = itemView;
        this.image = this.itemView.findViewById(R.id.pictureImageView);
        this.checkImage = this.itemView.findViewById(R.id.checkImageView);
        this.itemListener = itemListener;
        itemView.setOnClickListener(this);
    }

    /**
     * Image's getter.
     * @return the image.
     */
    public ImageView getImage() {
        return image;
    }

    /**
     * Card enabling setter.
     * @param isEnabled : true if enabled, false instead.
     */
    public void setEnabled(final boolean isEnabled){
        if(isEnabled){
            this.checkImage.setVisibility(View.GONE);
        } else {
            this.checkImage.setVisibility(View.VISIBLE);
        }
        this.itemView.setEnabled(isEnabled);
    }

    @Override
    public void onClick(View v) {
        this.itemListener.onItemClick(this.getAdapterPosition());
    }
}
