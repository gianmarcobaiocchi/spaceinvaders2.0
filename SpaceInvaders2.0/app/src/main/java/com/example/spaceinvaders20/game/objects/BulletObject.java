package com.example.spaceinvaders20.game.objects;

import com.example.spaceinvaders20.game.GameWorldThread;
import com.google.ar.sceneform.ArSceneView;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.Scene;
import com.google.ar.sceneform.collision.Ray;

/**
 * Bullet's class.
 */
public class BulletObject extends SimpleVirtualObject {

    private static final float SPEED = 0.005f;
    private static final int MAX_DISTANCE = 3;
    private final ArSceneView arSceneView;
    private final GameWorldThread world;
    private final Ray destRay;
    private float distance;

    /**
     * BulletObject's builder.
     * @param path : resource path.
     * @param arSceneView : ArSceneView where to build the 3D model.
     * @param world : the GameWorldThread in which is created.
     * @param destRay : the Ray on which it has to move.
     */
    public BulletObject(final String path, final ArSceneView arSceneView, final GameWorldThread world, final Ray destRay){
        super(path);
        this.arSceneView = arSceneView;
        this.world = world;
        this.destRay = destRay;
        this.distance = 0;
        super.getNode().setLookDirection(this.destRay.getDirection());
    }

    @Override
    public synchronized void destroy() {
        Node node = super.getNode();
        if(node != null){
            Scene scene = super.getNode().getScene();
            if (scene != null) {
                scene.removeChild(node);
            }
        }
    }

    @Override
    public void tick(long millis) {
        this.distance += SPEED * millis;
        if(this.distance <= MAX_DISTANCE){
            super.setPosition(this.destRay.getPoint(this.distance));
            Node nodeInContact = this.arSceneView.getScene().overlapTest(super.getNode());
            if(nodeInContact != null){
                int score = this.world.removeObject(nodeInContact);
                this.world.addPoints(score);
                this.world.removeObject(super.getNode());
            }
        } else {
            this.world.removeObject(super.getNode());
        }
    }
}
