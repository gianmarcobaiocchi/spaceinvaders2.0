package com.example.spaceinvaders20.game.objects;

import android.net.Uri;

import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Vector3;

/**
 * Game object's interface.
 */
public interface VirtualObject {

    /**
     * Set a new object's position.
     *
     * @param point : the point where to place the object
     */
    void setPosition(final Vector3 point);

    /**
     * Gets the source file uri.
     *
     * @return source's file uri
     */
    Uri getUri();

    /**
     * Gets the object's position.
     *
     * @return object's position
     */
    Vector3 getPosition();

    /**
     * Gets the node on which the object is rendered.
     * @return the object's node
     */
    Node getNode();

    /**
     * Runs a period function.
     */
    void tick(final long millis);

    /**
     * Removes the object from the scene
     */
    void destroy();
}
