package com.example.spaceinvaders20.game;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.example.spaceinvaders20.utilities.ArSceneHandler;
import com.example.spaceinvaders20.fragments.GameOverFragment;
import com.example.spaceinvaders20.R;
import com.example.spaceinvaders20.game.objects.BulletObject;
import com.example.spaceinvaders20.game.objects.SimpleVirtualObject;
import com.example.spaceinvaders20.game.objects.UfoObject;
import com.example.spaceinvaders20.utilities.FragmentHandler;
import com.example.spaceinvaders20.utilities.database.LeaderboardViewModel;
import com.example.spaceinvaders20.utilities.database.RankingPosition;
import com.google.ar.sceneform.math.Vector3;

import java.util.Optional;
import java.util.Random;

/**
 * Game handler class.
 */
public class PlayerGame {

    private static final int MAX_PERIOD_UFO = 1000; //milliseconds
    private static final int MIN_PERIOD_UFO = 500;
    private static final int MAX_UFOS = 10;
    private static final int MIN_UFOS = 20;
    private final Activity activity;
    private final GameWorldThread gameWorldThread;
    private final PlayerScore playerScore;
    private final ArSceneHandler arSceneHandler;
    private final LeaderboardViewModel viewModel;
    private Optional<View> shotButton;
    private Optional<TextView> scoreText;
    private final int nUfos;
    private int placedUfos;
    private long timeNextUfo;
    private long timePassed;

    /**
     * PlayerGame's builder.
     * @param activity : activity father.
     * @param playerName : player's name.
     * @param arSceneHandler : the AR fragment handler.
     */
    public PlayerGame(final Activity activity, final String pictureId, final String playerName, final ArSceneHandler arSceneHandler, final LeaderboardViewModel viewModel){
        this.activity = activity;
        this.gameWorldThread = new GameWorldThread(activity, this);
        this.playerScore = new PlayerScore(pictureId, playerName);
        this.arSceneHandler = arSceneHandler;
        this.viewModel = viewModel;
        this.shotButton = Optional.empty();
        this.scoreText = Optional.empty();
        this.nUfos = new Random().nextInt(MAX_UFOS) + MIN_UFOS;
        this.placedUfos = 0;
        this.timeNextUfo = 0;
        this.timePassed = 0;
    }

    /**
     * Initializes the new game.
     */
    public void init(){
        //Added city
        SimpleVirtualObject city = new SimpleVirtualObject("simpleCity.sfb");
        this.arSceneHandler.addObjectInScene(city);
        //Added cloud
        SimpleVirtualObject cloud = new SimpleVirtualObject("nuvola.sfb",
                new Vector3(0, SimpleVirtualObject.MAX_HEIGHT, 0));
        this.arSceneHandler.addObjectInScene(cloud);
        //Added first ufo
        this.generateUfo();
        this.arSceneHandler.placeCity(true);
    }

    /**
     * Starts the game.
     */
    public void start(){
        if(this.gameWorldThread.getState() == Thread.State.NEW){
            this.gameWorldThread.start();
            this.shotButton.ifPresent(view -> this.shotButton.get().setEnabled(true));
        }
    }

    /**
     * Pauses the game.
     */
    public void pause(){
        this.gameWorldThread.setInPause(true);
        this.shotButton.ifPresent(view -> this.shotButton.get().setEnabled(false));
    }

    /**
     * Resumes the game.
     */
    public void resume(){
        this.gameWorldThread.setInPause(false);
        this.shotButton.ifPresent(view -> this.shotButton.get().setEnabled(true));
    }

    /**
     * In pause getter.
     * @return if the game is in pause or not.
     */
    public boolean isInPause(){
        return this.gameWorldThread.isInPause();
    }

    /**
     * Stops the game.
     */
    public void gameOver(){
        if(this.placedUfos >= this.nUfos){
            this.gameWorldThread.setInPause(true);
            this.viewModel.addRankingPosition(new RankingPosition(this.playerScore.getPictureId(),
                                                                  this.playerScore.getPlayerName(),
                                                                  this.playerScore.getScore()));
            FragmentHandler.replaceFragment((FragmentActivity) this.activity, GameOverFragment.newInstance(this.playerScore));
        }
    }

    /**
     * Adds a shoot button listener.
     * @param button : shoot button to add the listener to.
     */
    public void addShootButton(final View button){
        this.shotButton = Optional.of(button);
        this.shotButton.ifPresent(view -> this.shotButton.get().setOnClickListener(v -> {
                                                BulletObject bullet = new BulletObject("laserBullet.sfb",
                                                                            this.arSceneHandler.getArSceneView(),
                                                                            this.gameWorldThread, this.arSceneHandler.getActualRay());
                                                this.arSceneHandler.addBulletInScene(bullet);
                                                this.gameWorldThread.addBullet(bullet);
                                            }));
    }

    /**
     * Adds the TextView on which print the score.
     * @param view : view to print the score on.
     */
    public void addTextViewScore(final TextView view){
        view.setText(this.activity.getString(R.string.score, this.playerScore.getScore()));
        this.scoreText = Optional.of(view);
    }

    /**
     * Adds the point to the player score.
     * @param points : points to add.
     */
    public void addPoints(final int points){
        this.playerScore.addScore(points);
        this.scoreText.ifPresent(view ->
                this.scoreText.get()
                              .setText(this.activity.getString(R.string.score, this.playerScore.getScore()))
        );
    }

    /**
     * Periodic function for manage the world.
     * @param timePassed : the time elapsed from the last tick function.
     */
    public void tick(final long timePassed){
        if(this.placedUfos < this.nUfos){
            this.timePassed += timePassed;
            if(this.timeNextUfo <= this.timePassed){
                this.generateUfo();
            }
        }
    }

    private void generateUfo(){
        float speed = (new Random().nextFloat() % UfoObject.MAX_SPEED) + UfoObject.MIN_SPEED; //Speed in meters/milliseconds
        int score = Math.round((speed / UfoObject.MAX_SPEED) * UfoObject.MAX_SCORE ) + UfoObject.MIN_SCORE;
        Vector3 degree = new Vector3(new Random().nextInt(UfoObject.MAX_ANGLE_DEGREES), 0,
                new Random().nextInt(UfoObject.MAX_ANGLE_DEGREES));
        float x = new Random().nextFloat() % (2 * UfoObject.POSITION_LIMIT) - UfoObject.POSITION_LIMIT;
        float y = SimpleVirtualObject.MAX_HEIGHT;
        float z = new Random().nextFloat() % (2 * UfoObject.POSITION_LIMIT) - UfoObject.POSITION_LIMIT;
        UfoObject ufo = new UfoObject("ufo.sfb", speed, degree, new Vector3(x, y, z), this.gameWorldThread, score);
        if(this.arSceneHandler.addObjectInScene(ufo)){
            this.gameWorldThread.addUfo(ufo);
            this.placedUfos++;
            this.timePassed = 0;
            this.timeNextUfo = new Random().nextInt(MAX_PERIOD_UFO - MIN_PERIOD_UFO) + MIN_PERIOD_UFO;
        }
    }
}
