package com.example.spaceinvaders20.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.spaceinvaders20.R;
import com.example.spaceinvaders20.game.PlayerScore;
import com.example.spaceinvaders20.utilities.FragmentHandler;
import com.example.spaceinvaders20.utilities.HomePagerAdapter;

/**
 * Game Over fragment.
 */
public class GameOverFragment extends Fragment {

    private static final String PLAYER_SCORE_KEY = "PLAYER_SCORE";
    private PlayerScore playerScore;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(this.getArguments() != null){
            this.playerScore = (PlayerScore) this.getArguments().getSerializable(PLAYER_SCORE_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.game_over_fragment, container, false);
        if(this.getActivity() != null){
            if(this.playerScore != null) {
                int pictureId = this.getActivity().getResources().getIdentifier(this.playerScore.getPictureId(), "drawable", this.getActivity().getPackageName());
                if(pictureId == 0) {
                    pictureId = R.drawable.picture_6;
                }
                ((ImageView) view.findViewById(R.id.pictureImageView))
                        .setImageResource(pictureId);
                //Set score text
                ((TextView) view.findViewById(R.id.scoreTextView))
                        .setText(this.getString(R.string.score, this.playerScore.getScore()));
                //Set name text
                ((TextView) view.findViewById(R.id.userNameTextView))
                        .setText(this.playerScore.getPlayerName());
            }
            //Set homeButton action listener
            view.setOnTouchListener((v, event) -> {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    view.performClick();
                    FragmentHandler.replaceFragment(this.getActivity(), HomeFragment.newInstance(HomePagerAdapter.HOME_INDEX));
                }
                return true;
            });
        }
        return view;
    }

    /**
     * Builder.
     * @param playerScore : player's score.
     * @return : new GameOverFragment instance.
     */
    public static GameOverFragment newInstance(final PlayerScore playerScore){
        GameOverFragment fragment = new GameOverFragment();
        Bundle args = new Bundle();
        args.putSerializable(PLAYER_SCORE_KEY, playerScore);
        fragment.setArguments(args);
        return fragment;
    }
}
