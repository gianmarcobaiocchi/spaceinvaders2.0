package com.example.spaceinvaders20.utilities.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Leaderboard table entity.
 */
@Entity(tableName = "leaderboard")
public class RankingPosition {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "position")
    private int id;
    @ColumnInfo(name = "id_picture")
    private String pictureId;
    @ColumnInfo(name = "playerName")
    private String playerName;
    @ColumnInfo(name = "score")
    private int score;

    /**
     * Builder.
     * @param pictureId : resource image id.
     * @param playerName : player name.
     * @param score : player score.
     */
    public RankingPosition(final String pictureId, final String playerName, final int score){
        this.pictureId = pictureId;
        this.playerName = playerName;
        this.score = score;
    }

    /**
     * Entity id getter.
     * @return the entity id.
     */
    public int getId(){
        return this.id;
    }

    /**
     * Entity picture id getter.
     * @return picture id.
     */
    public String getPictureId(){
        return this.pictureId;
    }

    /**
     * Player name getter.
     * @return player name.
     */
    public String getPlayerName(){
        return this.playerName;
    }

    /**
     * Player score getter.
     * @return player score.
     */
    public int getScore(){
        return this.score;
    }

    /**
     * Entity id getter.
     * @param id : entity id.
     */
    public void setId(int id) {
        this.id = id;
    }
}
